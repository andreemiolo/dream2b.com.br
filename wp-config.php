<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'dream2b');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x?JB~!d(lxpmh)}~irg1QeWm,:X&gM&$d?Bpxw0}>m;zdWCBict)pp<aW-xZl`Y.');
define('SECURE_AUTH_KEY',  'jMj5%&o+y[zWkg}O.:,#}tX` eTXDn68ljCI.Bg>@KNr1ll^E&|RJCdwp3B*w&[}');
define('LOGGED_IN_KEY',    '>tT )JV~9j8{e8?HrQn3bZ28EMakqW_=,}NNO/P{Y0/8IfY~^k{|ex;)%0c5mBxi');
define('NONCE_KEY',        'g~lc+zaoXGbF6GC|dDGWK8NO%vRC:|FFqsOVry22q2ouZ/2OKj!h3Fqk_y5epf[ ');
define('AUTH_SALT',        '|:H/J*sA55k8A_?w8Z>8}_ttPr%HHi4om`HJp]C4j{.MH_RDL(l*D-m3fZQ5#,N)');
define('SECURE_AUTH_SALT', '.wW:qDtzBp&JnXlCVFn3dZH+|/c[8!,U^kV.*Z^AF0Tr|C|Mzb7zAx@vCu(@#k5%');
define('LOGGED_IN_SALT',   'P{AL;kn6tu*_Tau,{|vAoIT1nK:HyOn/v5LVJ!AR:=~$wT_8N>gq4H:JVa+fwM_o');
define('NONCE_SALT',       'n>x9|-D:K[ivI50f4KW;mm^>9>Y)[ZGthp84T2pjs=L0>5~k?(v6U{w@lA Z?Sx}');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
